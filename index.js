import startBot from "./src/bot.js";

const bot = startBot({
  tgApiToken: process.env.TG_API_TOKEN
});
