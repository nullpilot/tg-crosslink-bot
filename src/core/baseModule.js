export default class BaseModule {
  constructor(bot, opts = {}) {
    const moduleName = this.constructor.name
    const moduleKey = moduleName.replace(/Module$/, "").toLowerCase();

    if(!bot.context.hasOwnProperty("modules")) {
      bot.context.modules = {};
    }

    this.bot = bot;
    this.opts = opts;
    bot.context.modules[moduleKey] = this;

    console.log(`Loading module ${moduleName} at ctx.modules.${moduleKey}`);
  }

  static async start(bot, opts) {
    const module = new this(bot, opts);

    await module.init(bot);
    await module.middleware(bot);
    await module.commands(bot);

    return module;
  }

  init() {}
  middleware() {}
  commands() {}
}
