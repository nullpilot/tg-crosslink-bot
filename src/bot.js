import { Telegraf } from "telegraf";
import ErrorModule from "./modules/error.js";
import InfoModule from "./modules/info.js";
import CrosslinkModule from "./modules/crosslink.js";
import { isCommand, argumentMiddleware } from "./core/util.js";

export default async function startBot({ tgApiToken, ...opts }) {
  console.log("Initializing Telegram bot..");

  const bot = new Telegraf(tgApiToken, { retryAfter: 2 });
  const botInfo = await bot.telegram.getMe();

  bot.options.username = botInfo.username;
  bot.context.state = {};

  // Provide ctx.arg and ctx.argv for all commands
  bot.use(Telegraf.optional(isCommand, argumentMiddleware));

  console.log("Loading modules..");
  await ErrorModule.start(bot);
  await InfoModule.start(bot);
  await CrosslinkModule.start(bot);

  bot.startPolling();

  console.log(`Started Telegram bot @${bot.options.username}!`);

  // Enable graceful stop
  process.once('SIGINT', () => bot.stop('SIGINT'));
  process.once('SIGTERM', () => bot.stop('SIGTERM'));

  return bot;
}
