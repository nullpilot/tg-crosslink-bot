import got from "got";

export async function linksFromUrl(url) {
  const res = await got.get('https://api.song.link/v1-alpha.1/links', {
    responseType: 'json',
    searchParams: {
      url: encodeURIComponent(url)
    }
  });

  if(res.statusCode !== 200) {
    throw new Error(res.body);
  }

  const songInfo = getSongInfo(res.body);

  return {
    info: songInfo,
    links: res.body.linksByPlatform,
    entities: res.body.entitiesByUniqueId
  };
}

const PROVIDER_PREFERENCE = ["appleMusic", "spotify", "tidal", "deezer", "youtube"];
export function getSongInfo(res) {
  const entities = Object.values(res.entitiesByUniqueId);
  const infoMap = PROVIDER_PREFERENCE.map(provider => {
    return entities.find(e => e.platforms.indexOf(provider) >= 0);
  });

  return infoMap.shift();
}
