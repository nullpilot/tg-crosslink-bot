import { Composer } from "telegraf";
import BaseModule from "../core/baseModule.js";
import { contextReply, render } from "../core/util.js";

const {
  command,
  privateChat
} = Composer;

export default class InfoModule extends BaseModule {
  commands(bot) {
    bot.use(
      privateChat(command(["start", "info"], this.sendInfo))
    );
  }

  async sendInfo(ctx) {
    const msg = await render("info");

    await contextReply(ctx, msg, {
      parse_mode: "HTML",
      disable_web_page_preview: true
    });
  }
}
