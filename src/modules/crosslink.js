import { Composer, Markup } from "telegraf";
import BaseModule from "../core/baseModule.js";
import { contextReply, render } from "../core/util.js";
import { linksFromUrl } from "../songlink.js";

const {
  command,
  privateChat,
  url
} = Composer;

export default class CrosslinkModule extends BaseModule {
  commands(bot) {
    const crosslink = this.crosslink.bind(this);

    bot.use(
      privateChat(url(crosslink))
    );
  }

  async crosslink(ctx) {
    const urlEntity = ctx.message.entities.filter(e => e.type === 'url')[0];
    const url = ctx.message.text.substr(urlEntity.offset, urlEntity.length);

    const songData = await linksFromUrl(url);
    const msg = await render("crosslink", songData);
    const buttons = [
      optionalPlatformButton("Spotify", "spotify", songData.links),
      optionalPlatformButton("Apple Music", "appleMusic", songData.links),
      optionalPlatformButton("Youtube", "youtube", songData.links),
      optionalPlatformButton("Tidal", "tidal", songData.links),
      optionalPlatformButton("Deezer", "deezer", songData.links),
      optionalPlatformButton("Soundcloud", "soundcloud", songData.links)
    ].filter(b => b);

    const keyboard = Markup.inlineKeyboard(buttons, {
      wrap: (btn, i) => {
        if(buttons.length <= 3) {
          return false;
        } else if(buttons.length === 4) {
          return i % 2 === 0;
        } else if(buttons.length === 5) {
          return ((i - 5) % 3 == 0);
        } else {
          return i % 3 === 0;
        }
      }
    })

    await ctx.replyWithPhoto(songData.info.thumbnailUrl, {
      caption: msg,
      parse_mode: "HTML",
      disable_web_page_preview: true,
      ...keyboard
    });
  }
}

function optionalPlatformButton(platform, id, links) {
  if(!links.hasOwnProperty(id)) {
    return null;
  }

  return Markup.button.url(`🎵  ${platform}`, links[id].url);
}
