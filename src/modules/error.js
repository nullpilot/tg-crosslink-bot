import BaseModule from "../core/baseModule.js";
import { contextReply, render } from "../core/util.js";

export default class ErrorModule extends BaseModule {
  middleware(bot) {
    bot.use(this.errorHandlingMiddleware);
  }

  async errorHandlingMiddleware(ctx, next) {
    try {
      await next(ctx);
    } catch(error) {
      if(ctx.state.isClientError) {
        await contextReply(ctx, error.message || error, {
          disable_web_page_preview: true
        });

        ctx.state.isClientError = false;
      } else {
        const msg = await render("error", {
          error: error.message || error
        });

        await contextReply(ctx, msg, {
          parse_mode: "HTML",
          disable_web_page_preview: true
        });

        console.log("Uncaught error:");
        console.error(error);
      }
    }
  }
}
